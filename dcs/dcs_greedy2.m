function [S, S_density, result] = dcs_greedy2(G)


m = length(G); 

for i=1:m
    G{i}= double(G{i}); 
end

n = size(G{1}, 1); 
sort_deg = zeros(n, m); 
sort_idx = zeros(n, m);
for i=1:m
    cG = G{i}; 
    cdeg  = sum(cG, 2); 
    [csort_deg, csort_idx] = sort(cdeg, 'descend'); 
    sort_deg(:, i) = csort_deg; 
    sort_idx(:, i) = csort_idx; 
end


common_density = @(G, S) min(cellfun(@(x) nnz(x(S, S))/(2*length(S)), G )); 
node_degree = @(i, G, S) cellfun(@(x) nnz(x(i, S)), G ); 
V = [1:n]';  
cds_density = zeros(n, 1); 
t = 1; 
i_t = zeros(n, 1); 
m_t = zeros(n, 1); 
r_t = zeros(n, 1); 
k_t = zeros(n, 1); 
z_t = zeros(n, 1); 
deg_plus = zeros(n, 1); 
deg_minus = zeros(n, 1); 
for j=1:(n-1)
    
    V_t = setdiff(V , i_t); 
    cds_density(j) = common_density(G, V_t); 
    [md, ind] = graphset_min_degree(G, V_t); 
    [deg_m, m_t(j)] = min(md); 
    k_min = ind{m_t(j)};    
    i_t(j) = k_min(1); 
    
    G_t = G{m_t(j)}; 
    % maximum degree of this node in other graphs 
    deg_1v = node_degree(i_t(j), G, V_t); 
    deg_plus(j) = max(deg_1v); 
    
    for ki=1:length(k_min)
        k1 = k_min(ki); 
        c_deg_v = node_degree(k1, G, V_t); 
        c_max = max(c_deg_v); 
        if c_max < deg_plus(j)
            deg_plus(j) = c_max; 
            i_t(j) = k1; 
        end
    end
    
    % minimum degree node in this graph 
    deg_2v = sum(G_t(V_t, V_t), 2); 
    deg_minus(j) = min( deg_2v(deg_2v > 0));         



    V_1 = find(deg_2v == 0);
    k_t(j) = deg_plus(j) / deg_minus(j);     
    z_t(j) = length(V_t) / length(V_1); 
    
    r_t(j) = k_t(j) * z_t(j); 
    
    fprintf(2, '%d/%d i_t: %d deg_plus: %d density: %g\n', j, n, i_t(j), deg_plus(j), cds_density(j)); 
end

[best_density , best_idx] = max(cds_density); 
S = setdiff(V, i_t(1:(best_idx-1))); 
r = max(r_t);  
 

% which graph has currently minimum density  
[S_density, S_idx] = common_density(G, S); 
assert(abs(S_density - best_density ) < 1e-6); 

result = struct('i_t', i_t, 'density', cds_density, 'r_t', r_t, 'z_t' , z_t, 'm_t', m_t, 'k_t', k_t);
fprintf(2, 'DCSGREEDY2: Extracted subgraph of size %d and density %g\n', length(S), S_density); 
