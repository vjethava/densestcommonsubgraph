function dimacs_comparison() 
% This file compares the densest common subgraph obtained using three approximate methods
%
% MKL (SVM-THETA)
% DCS-LP
% CHARIKAR(MIN)
% CHARIKAR(AVG)
% 

res_dir = [getenv('SVM_THETA_HOME') '/results/'];
dimacs_res_file = [res_dir '/result-mult-dimacs-with-sparse-check.mat']; 
load(dimacs_res_file); % G, alpha, 

load([res_dir '/dimacs-mkl-normalized-laplacian.mat']); 

cmp_res_file = [res_dir '/dimacs-mkl-laplacian-comparison.mat']; 

solvers = {'MKL'; 'DCSP' ;  'MKL-LAPLACIAN' }; % 1 => MKL
                                               % 2 => DCS

graph_names = DIMACS.graph_sets ;
rounding_methods = {'YPOS', 'GREEDY' };  % 1 choose S = {y_i > 0} 
                                         % 2 choose S greedy when sorted by y_i 


ng = length(graph_names);
ns = length(solvers); 
nr = length(rounding_methods); 

% parfor gi=1:ng   % choose graph set and run DCSP 
%     curr_graph_set = G{gi}; 
%     curr_graph_set_name = graph_names{gi};     
%     [t, x, y, cvx_status] = DCSP(curr_graph_set); 
%     curr_dcsp_file = [res_dir '/dcsp/' curr_graph_set_name '.mat'];
%     parsave(curr_dcsp_file, t, x, y, cvx_status, curr_graph_set); 
% end


% saves the following information: graph_set, solver, y,  rounding, S, |S|, d   
rounding_result = cell(ng * ns * nr, 7); 

% do rounding and save in rounding_result 
for gi=1:ng
    curr_graph_set = G{gi};
    curr_graph_set_name = graph_names{gi}; 
    for si=[1 3] % choose solver
        curr_id = ns*(gi-1) + si; 
        curr_solver = solvers{si};
        if si == 1 % MKL solver
            curr_y = alpha{gi}; 
        elseif si == 2  % DCSP 
            dcs_res_file = [res_dir  '/dcsp/' curr_graph_set_name '.mat']; 
            load(dcs_res_file, 'y'); 
            curr_y = y; clear y; 
            assert(length(curr_y) == size(curr_graph_set{1}, 1)); 
        elseif si == 3
            curr_y = alpha_mkl{gi};
        else            
            error('unknown solver'); 
        end
        % fprintf(2, '%s %s ny: %d\n', curr_graph_set_name, curr_solver, length(curr_y)); 
        for ri=1:nr % choose rounding 
            S = [];
            d = 0.0; 
            [S, d] = dcs_rounding(curr_graph_set, curr_y, ri); 
            rr_id = nr * (curr_id-1) + ri;  
            
            rounding_result{rr_id, 1} = curr_graph_set_name;
            rounding_result{rr_id, 2} = curr_solver; 
            rounding_result{rr_id, 3} = curr_y;
            rounding_result{rr_id, 4} = rounding_methods{ri};             
            rounding_result{rr_id, 5} = S;
            rounding_result{rr_id, 6} =length(S);
            rounding_result{rr_id, 7} =  d; 
        end
    end
    fprintf(2, 'saving rounding_result of graph set %s to %s\n', curr_graph_set_name, cmp_res_file); 
    save(cmp_res_file, 'rounding_result'); 
end








