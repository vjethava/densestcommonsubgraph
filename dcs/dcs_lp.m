function [t, result] = dcs_lp(A) 
% DCS_LP Solves the Densest Common Subgraph (DCS) LP 
%        given by:
% 
%          max_{x,y,t}  t 
%          s.t.          \sum_{ij\in E^m} x^{m}_{ij} >= t  \forall [m] 
%                        x^{m}_{ij} <= y_{i}
%                        x^{m}_{ij} <= y_{j}  \forall ij\in E^{m}
%                        \sum_{i} y_{i} <= 1  
%                        x^{m}_{ij}, y_{i} >=  0
%
% 
% USAGE: [t, result] = dcs_lp(G) 
% 
% RETURNS: 
% --------
% t: objective value of the LP
% result:  structure containing: 
%      result.x: primal variables
%      result.y: primal variables 
%      result.dual: dual variables 
%
if ~exist('cvx_version', 'file')
    error('dcs_lp(): Set-up CVX first!'); 
end

m = length(A); 
n = size(A{1}, 1); 
fprintf(2, 'starting LP setup\n') ;
cvx_quiet false ;

cvx_begin 
variable x(n, n, m) nonnegative; 
variable y(n) nonnegative; 
dual variables alpha1{n, n, m};
dual variables lambda1{m}; 
dual variables beta1{n, n, m}; 
dual variable gamma1; 
variable t; 
maximize t
subject to 
gamma1: sum(y) <= 1
for i=1:m
    lambda1{i}: trace(A{i} * x(:, :, i)) >= t  
    [ai, aj, av] = find(A{i}); 
    for e=1:length(ai)
        crow = ai(e); 
        ccol = aj(e); 
        alpha1{crow, ccol, i}: x(ai(e), aj(e), i) <= y(ai(e)) 
        beta1{crow, ccol, i}: x(ai(e), aj(e), i) <= y(aj(e))
    end
end
cvx_end

result = struct();
result.x = x;
result.y = y; 
dual1 = struct();

dual1.obj = gamma1; 
dual1.alpha = alpha1;
dual1.beta = beta1; 
dual1.lambda = lambda1; 
    
result.dual = dual1; 