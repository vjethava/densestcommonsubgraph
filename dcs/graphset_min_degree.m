function [min_degrees, indices ] = graphset_min_degree(G, S)
m = length(G); 
if nargin < 2
    n = size(G{1}, 1); 
    S = [1:n]';
end

n = length(S); 
indices = cell(m, 1);
degrees = zeros(m, n); 
min_degrees = zeros(m, 1); 
for i=1:m
    cG = G{i}; 
    degrees(i, :) = sum(cG(S, S), 1); 
    [v, k] = min(degrees(i, :)); 
    indices{i} = S(find(degrees(i, :) == v));  
    min_degrees(m) = v; 
end
