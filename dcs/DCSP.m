function [cvx_optval, x, y, cvx_status] = DCSP(G) 
m = length(G); 
n = size(G{1}, 1); 


ei = []; 
ng = zeros(m, 1);  
for i=1:m 
    ci = find(G{i}) ;
    ei = [ei; ci]; 
    ng(i) = length(ci); 
end
ne = length(ei); 


getr = @(e) floor( ( e - 1) / n ) + 1; 
getc = @(e) mod( (e - 1) , n ) + 1; 


getgb = @(m) sum(ng(1:(m-1))) + 1;
getge = @(m) sum(ng(1:m)); 

cvx_quiet false; 

cvx_begin

cvx_precision low; 
variable x(ne, 1) nonnegative;  
variable y(n, 1) nonnegative; 
variable t nonnegative;

maximize t
subject to 
sum(y) <= 1.0; 

er = getr(ei); 
ec = getc(ei); 

x <= y(er); 
x <= y(ec); 

for i=1:m 
    gb = getgb(i); 
    ge = getge(i);
    sum(x(gb:ge)) >= t; 
end

cvx_end
