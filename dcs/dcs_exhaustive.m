function [max_min_subgraph, max_min_degree] = dcs_exhaustive(G)
% DCS_EXHAUSTIVE Does naive exhaustive search for densest common subgraph
%  
% Usage: [max_min_subgraph, max_min_degree] = dcs_exhaustive(G)
% 
% Expects:
% --------
% G (mx1) cell where G{i} is the adjacency matrix for the i^th subgraph
% 
% Returns: 
% --------
% 
% Note:
% -----
% Minor efficiency update: See https://jamesmccaffrey.wordpress.com/2008/12/08/a-gray-code-mini-library-in-c/
% 
% 
VERBOSE_MODE = 0; 
m = length(G); 
n = size(G{1}, 1); 
max_min_degree = 0; 
max_min_idx = 0; 
curr_degrees = zeros(1, m); 

if VERBOSE_MODE
    h = waitbar(0);
end


curr_S = []; 
prev_S = [];

curr_num_edges  = zeros(m, 1); 
curr_degrees = zeros(m, 1); 

prev_ss = repmat('0', 1, n);
prev_ns = 0; 

for i=1:(2^n-1)
    % here we have inlined dec2bin
    bb = char(rem((i*pow2(1-n:0)),2)+'0');
    
    %% using Gray codes - the previous state and current state differ by atmost 1 node
    curr_ss = my_bin2gray(bb); 
    curr_S = find(curr_ss == '1') ;
    curr_ns = length(curr_S); 
    curr_node = find(prev_ss ~= curr_ss);
    assert(abs(prev_ns - curr_ns) == 1, 'Invalid Gray coding!'); 
    for j=1:m
        if curr_ns > prev_ns % node added
            curr_num_edges(j) = curr_num_edges(j) + sum(G{j}(prev_S, curr_node)) + sum(G{j}(curr_node, prev_S)); 
        else
            curr_num_edges(j) = curr_num_edges(j) - sum(G{j}(prev_S, curr_node)) - sum(G{j}(curr_node, prev_S)); 
        end
    end
    curr_degrees = curr_num_edges ./ curr_ns;   
    curr_min_degree = min(curr_degrees); 
    
    if VERBOSE_MODE
        waitbar(i/(2^n-1), h);     
        fprintf(2, 'S: %s %s curr_min_d: %5.3g best_d: %5.3g\n', bb, curr_ss, curr_min_degree, max_min_degree); 
    end    
    if  max_min_degree < curr_min_degree
        max_min_degree = curr_min_degree; 
        max_min_idx = i; 
    end
    
    prev_S = curr_S; 
    prev_ss = curr_ss; 
    prev_ns = curr_ns; 
    
end
if VERBOSE_MODE
    close(h); 
end

max_min_subgraph = find(my_bin2gray(dec2bin(max_min_idx, n)) == '1'); 
fprintf(2, 'dcs_exhaustive(): max_min_degree: %g for subgraph\n', max_min_degree); 
disp(max_min_subgraph); 


function [g] = my_bin2gray(b)
n =  length(b);
g = repmat('0', 1, n); 
g(1) = b(1); 
for i=(n):-1:2
    if strcmp(b((i-1):(i)), '01') || strcmp(b((i-1):i), '10')
        g(i) = '1';
    end
end

