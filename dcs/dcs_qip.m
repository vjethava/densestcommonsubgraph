function [t, x] = dcs_qip(G)
% DCS_QIP Returns Densest Common Subgraph for graphs G{1} ... G{m} 
%         by solving the Quadratic Integer Program 
%         
%         max_{x, t}  t 
%         s.t. 
%                     x^\top (A^{(j)} - tI) x >= 0 \forall 1\le j \le m 
%                     x_i \in {0, 1} \forall i\in [n]
% 
% Usage: [S] = dcs_qip(G)
%
% Expects:
% --------
% G: cell (mx1) containing nxn sparse matrices (each representing one graph) 
%
% Returns: 
% --------
% S - index of the nodes in the common densest subgraph
% densities - mx1 vector showing density of induced subgraph in G{i} 
%%
    m = length(G); 
    n = size(G{1}, 1); 
    cvx_clear; 
    cvx_begin
        variable x(n, 1);
        variable t; 
        variable y; 
        maximize t
        subject to
        for i=1:m
          -quad_form(x, G{i}) + t*y <= 0;
        end
        sum(x) - y == 0; 
        for j=1:n
            x(i)*x(i) - x(i) == 0; 
        end        
    cvx_end
end    
        