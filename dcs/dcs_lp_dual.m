function [t, result] = dcs_lp_dual(G) 
% DCS_LP Solves the Densest Common Subgraph (DCS) LP 
% 
% Usage: [x, y, t, S] = dcs_lp(G) 
% 
if ~exist('cvx_version', 'file')
    error('dcs_lp(): Set-up CVX first!'); 
end

m = length(G); 
n = size(G{1}, 1); 

cvx_begin 
    variable a(n, n, m) nonnegative; 
    variable b(n, n, m) nonnegative; 
    variable z(n, m);    
    variable t;    % gamma 
    variable l(m) nonnegative; % lambda
    minimize t
    subject to 
        sum(l) >= 1
        for i=1:m
            cG = G{i}; 
            
            for ci=1:n
                for cj=1:n
                    if cG(ci, cj) > 0 
                        a(ci, cj, i) + b(ci, cj, i) >= l(i)
                    end
                    %                    else
                    %    a(ci, cj, i) == 0
                    %    b(ci, cj, i) == 0
                    % end
                end
            end
        end
        
        for i=1:n
            for k=1:m
                cG = G{k}; 
                z(i, k)  == cG(i, : ) * a(i, :, k)' + b(:, i, k)' * cG(:, i) 
            end
            t  >= sum(z(i, :)); 
        end        
cvx_end
result = struct(); 
result.gamma = t; 
result.lambda = l; 
result.alpha = a; 
result.beta = b; 
