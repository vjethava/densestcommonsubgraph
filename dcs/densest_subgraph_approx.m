function [S, best_density, result] = densest_subgraph_approx(G, verbose_mode)
% DENSEST_SUBGRAPH_APPROX Returns 2 approximation to densest subgraph in an 
% undirected graph using greedy method described in [Charikar, 2000]
% 
% Usage: [S, best_density, S_rel, relative_density, result] = densest_subgraph_approx(G, verbose_mode) 
%
% Also returns subgraph with best relative density but no guarantees there. 
%
% Expects: 
% --------
% G:             Input graph
% verbose_mode:  Whether to show progress bar (defualt = 1) or not (=0)
%
% Returns: 
% --------
% S:             Subgraph with best |E(S)|/|S|
% best_density:  density for S (|E(S)|/|S|)
% result:        Structure containing: 
%                     result.S : order in which nodes are removed 
%                     -- result(j).density: density of subgraph after removing nodes in result(1:j).S 
%                     -- result(j).relative_density: relative density of result(1:j).S
%
% Note: 
% -----
% "Greedy approximation algorithms for finding dense components in a graph", Moses Charikar. 
%  Approximation Algorithms for Combinatorial Optimization, 84-95, 2000. Springer. 
% 

if nargin < 2
    verbose_mode = 1; 
end
fprintf(2, 'densest_subgraph_approx(): Finding dense subgraph using greedy approach [Charikar, 2000]\n');  
G = double(G); % FIXME: Hack to handle Graph class. 
n = size(G, 1); 
deg = sum(G, 2); 
[sort_deg, sort_idx] = sort(deg, 'descend'); 

Sv = zeros(n, 1); 
dv = zeros(n, 1); 
rdv = zeros(n, 1); 

dv(n) = sum(sort_deg)/n;
rdv(n) = dv(n) /(n-1); 

S = []; 
best_density = 0.0; 
fprintf(2, 'densest_subgraph_approx(): Starting loop (n=%d) ...\n', n); 
if verbose_mode
    h = waitbar(0); 
end

for j=1:(n-2)    
    % size of current subgraph
    i = n - j + 1; 
    % remove current node and note its neighbours 
    curr_node_to_remove = sort_idx(i); 
    curr_node_ngbrs = G(sort_idx, curr_node_to_remove); 
    
    % update the degrees for the neighbours of removed node 
    sort_deg = sort_deg - curr_node_ngbrs; 
    sort_deg = sort_deg(1:(i-1)); 
    
    % resort the degrees if needed 
    if ~issorted(sort_deg(end:-1:1))
        [new_sort_deg, new_sort_idx] = sort(sort_deg, 'descend'); 
        sort_deg = new_sort_deg; 
        sort_idx = sort_idx(new_sort_idx);
    else
        sort_idx = sort_idx(1:(end-1)); 
    end
    
    % update best subgraph if current subgraph has higher density
    curr_S = sort_idx; 
    curr_density = sum(sort_deg) / (i-1); 
    curr_relative_density = curr_density / (i - 2);
    if curr_density  >= best_density 
        S = curr_S; 
        best_density = curr_density; 
    end
 
    % update history for algorithm run with current subgraph
    Sv(i) = curr_node_to_remove; 
    dv(i-1) = curr_density; 
    rdv(i-1)= curr_relative_density; 
    
    if verbose_mode 
        msg = sprintf('j: %d/%d |S|: %d density: %g', j, (n-2), length(S), best_density); 
        waitbar(j/(n-2), h, msg); 
    end 
end
if verbose_mode
    close(h); 
end

S = sort(S); % return node indices in sorted order. 
fprintf(2, 'densest_subgraph_approx(): Found dense subgraph of size %d with density (|E(S)|/|S|): %.3g\n', ...
        length(S), best_density); 

Sv(1:2) = sort_idx;
result = struct('S', Sv, 'density', dv, 'relative_density', rdv); 


function [f_S] = get_subgraph_degree(G, idx)
% Return the density of subgraph indexed by idx 
n_S = length(idx); 
S = G(idx, idx); 
E_S = sum(sum(S)); 
f_S = E_S / (2*n_S); 