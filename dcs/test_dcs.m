%
% 
clc; close all; clear all; 

n = 30; 
p = 0.1; 
k = 4; 

G = {};
G{1} = erdosRenyi(n, p); 
G{2} = getPlantedClique(n, p, k); 
% tic; 
% [s, d] = dcs_exhaustive(G) ;  
% t_exhaustive = toc; 

ne = sum(cellfun(@nnz, G))/2; 

tic; 
[obj1, result] = dcs_lp(G); 
y1 = result.y;   
t_1 = toc; 

tic; 
[obj2, x2, y2] = DCSP(G); 
t_2 = toc; 

fprintf(2, 'n: %d ne: %d t_1: %g t_2: %g\n', n, ne, t_1, t_2); 
assert(abs(obj1 - obj2) <= 1.0e-6); 
assert(norm(y1 - y2, Inf) <= 1.0e-6); 
