function [S, S_density, r, result] = dcs_greedy1(G)
m = length(G);
for i=1:m
    G{i}= double(G{i});
end

n = size(G{1}, 1); 
sort_deg = zeros(n, m); 
sort_idx = zeros(n, m);
for i=1:m
    cG = G{i}; 
    cdeg  = sum(cG, 2); 
    [csort_deg, csort_idx] = sort(cdeg, 'descend'); 
    sort_deg(:, i) = csort_deg; 
    sort_idx(:, i) = csort_idx; 
end
result = []; 
r = 0; 

V = [1:n]';  
cds_density = zeros(n, 1); 
t = 1; 
i_t = zeros(n, 1); 
m_t = zeros(n, 1); 
r_t = zeros(n, 1); 
k_t = zeros(n, 1); 
z_t = zeros(n, 1); 
deg_plus = zeros(n, 1); 
deg_minus = zeros(n, 1); 
H = GraphSet(G); 
S_density = 0; 
S = []; 
for j=1:(n-1)
    % common density of the current subgraph 
    [curr_cds, min_graph_idx] = min(H.density) ;     
    if curr_cds > S_density
        S_density = curr_cds;
        S = H.S; 
    end
    
    cds_density(j) = curr_cds; 
    
    curr_deg_list = H.degree_list(:, min_graph_idx); 
    num_nodes_of_each_degree =  cellfun(@length, curr_deg_list); 
    deg_of_min_deg_node = min(find(num_nodes_of_each_degree)) - 1;
    set_of_min_deg_nodes = curr_deg_list{deg_of_min_deg_node + 1}; 
    

    % deg_minus(j) = min(find( num_nodes_of_each_degree(2:end) ) ); 
    
    curr_node = set_of_min_deg_nodes(1); 
    c_max_deg = H.get_max_degree_across_graphs(curr_node);
    % % for k=2:length( set_of_min_deg_nodes )
    % %     other_node = set_of_min_deg_nodes(k); 
    % %     o_max_deg = H.get_max_degree_across_graphs(other_node);
    % %     if o_max_deg < c_max_deg 
    % %         c_max_deg = o_max_deg; 
    % %         curr_node = other_node; 
    % %     end
    % % end
    
    % deg_plus(j) = c_max_deg; 
    
    % n0 = length( curr_deg_list{1}); 
        
    % k_t(j) = deg_plus(j) / deg_minus(j);     
    % z_t(j) = H.ns / (H.ns - n0);
    % m_t(j) = min_graph_idx; 
    % i_t(j) = curr_node; 
    
    % r_t(j) = k_t(j) * z_t(j); 
    
    % fprintf(2, '%d/%d m_t: %d i_t: %d density: %g\n', j, n, m_t(j),  i_t(j), cds_density(j)); 
    H.remove_node(curr_node); 
end

% r_t = r_t(~isinf(r_t)); 
% r = max(r_t); 

% [best_density , best_idx] = max(cds_density); 
% S = setdiff(V, i_t(1:(best_idx-1))); 
 

% common_density = @(G, S) min(cellfun(@(x) nnz(x(S, S))/(2*length(S)), G )); 
% node_degree = @(i, G, S) cellfun(@(x) nnz(x(i, S)), G ); 

% which graph has currently minimum density  
% [S_density, S_idx] = common_density(G, S); 
% assert(abs(S_density - best_density ) < 1e-6); 

% result = struct('i_t', i_t, 'density', cds_density, 'r_t', r_t, 'z_t' , z_t, 'm_t', m_t, 'k_t', k_t); 
fprintf(2, 'DCSGREEDY1: Extracted subgraph of size %d and density %g\n', length(S), S_density); 
