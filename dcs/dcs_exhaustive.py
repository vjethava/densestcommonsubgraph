import time
from timeit import timeit
import numpy as np
import networkx as nx
import math
import binstr
from binstr import b_bin_to_gray, b_gray_to_bin
from tqdm import trange

def erdos_renyi_graph(n, p, directed=False):
    G = np.random.rand(n, n) < p
    for i in range(n):
        for j in range(i, n):
            if i==j:
                G[i, j] = 0
            elif directed:
                G[i, j] = int(G[i, j])
                G[j, i] = int(G[j, i])
            else:
                G[i, j] = int(G[i, j])
                G[j, i] = G[i, j] 
    return G

def planted_clique_graph(n, p, k):
    G = erdos_renyi_graph(n, p, directed=False)
    for i in range(k): 
        for j in range(i+1, k):  
            G[i, j] = 1
            G[j, i] = 1
    return G

def get_test_graphs(n, p, k, seed=None): 
    # First graph 
    G = []
    G1 = erdos_renyi_graph(n, p)
    G2 = planted_clique_graph(n, p, k)
    # Second graph 
    G.append(G1)
    G.append(G2)
    return G

def get_gray_code(i, n):
    b = np.binary_repr(i, n)
    g = binstr.b_bin_to_gray(b)
    return g

def find_set_bits(b):
    S = []
    for i in range(len(b)):
        if b[i] == '1':
            S.append(i)
    return S

def dcs_exhaustive(G):
    m = len(G)
    n = G[0].shape[0]

    prev_state = '0'*n
    prev_nodes = []
    curr_num_E = np.zeros((m, 1))
    curr_deg = np.zeros((m, 1)) 
    
    best_min_deg = 0
    best_state= '0' * n
    
    for i in range(1, 2**n):
        curr_state = get_gray_code(i, n)
        curr_nodes = find_set_bits(curr_state)
        curr_ns = len(curr_nodes)
        delta_node = filter(lambda i: curr_state[i] != prev_state[i], range(n))[0]
        if curr_state[delta_node] == '0': # node deleted
            m_sign = -1
        else:                             # node added 
            m_sign = +1
        for j in range(m):
            delta_deg = G[j][prev_nodes, delta_node].sum(axis=None) + G[j][delta_node, prev_nodes].sum(axis=None) 
            curr_num_E[j] = curr_num_E[j] + m_sign * (delta_deg)
            curr_deg[j] = curr_num_E[j] / curr_ns 
        # minimum among the different graphs, the density of the induced subgraph 
        curr_min = np.min(curr_deg)
        # update if current minimum is higher than previous best
        if curr_min > best_min_deg:
            best_min_deg = curr_min
            best_state = curr_state
        # book-keeping 
        prev_state = curr_state
        prev_nodes = curr_nodes
    return (best_state, best_min_deg)
            
    
def main(n=15):
    p = 1.0/n
    k = 4
    G = get_test_graphs(n, p, k, 0)
    (best_state, best_min_deg) = dcs_exhaustive(G)
    print(best_state)
    print(best_min_deg)
    
if __name__=="__main__":
    t = time.time()
    main(20)
    print 'Elapsed: %s\n' % (time.time() - t) 
