function [S, S_density] = greedy2v1(G)


m = length(G); 

for i=1:m
    G{i}= double(G{i}); 
end

n = size(G{1}, 1); 
sort_deg = zeros(n, m); 
sort_idx = zeros(n, m);
for i=1:m
    cG = G{i}; 
    cdeg  = sum(cG, 2); 
    [csort_deg, csort_idx] = sort(cdeg, 'descend'); 
    sort_deg(:, i) = csort_deg; 
    sort_idx(:, i) = csort_idx; 
end


common_density = @(G, S) min(cellfun(@(x) nnz(x(S, S))/(2*length(S)), G )); 
node_degree = @(i, G, S) cellfun(@(x) nnz(x(i, S)), G ); 
V = [1:n]';  
cds_density = zeros(n, 1); 
t = 1; 
i_t = zeros(n, 1); 
m_t = zeros(n, 1); 
r_t = zeros(n, 1); 
k_t = zeros(n, 1); 
z_t = zeros(n, 1); 
deg_plus = zeros(n, 1); 
deg_minus = zeros(n, 1); 
H = GraphSet(G); 
removed = [];
S_density = 0; S = [] ; 
for j=1:(n-1)
    [curr_cds, m_1]= min(H.density); 
    if curr_cds > S_density
        S_density = curr_cds; 
        S = H.S; 
    end
    [min_deg_v, min_idx_v] = H.min_degree_set(); 
    [min_deg, m_2] = min(min_deg_v);     
    nodes_with_min_deg_in_G_m_2 = min_idx_v{m_2}; 
    
    curr_node = nodes_with_min_deg_in_G_m_2(1);  
    c_max_deg = H.get_max_degree_across_graphs(curr_node);
    for k=2:length(nodes_with_min_deg_in_G_m_2)
        other_node = nodes_with_min_deg_in_G_m_2(k); 
        o_max_deg = H.get_max_degree_across_graphs(other_node);
        if o_max_deg < c_max_deg 
            c_max_deg = o_max_deg; 
            curr_node = other_node; 
        end
    end
    
    
    H.remove_node(curr_node); 
    removed = [removed; curr_node]; 
    remaining = setdiff([1:n], removed); 
    fprintf(2, '%d/%d node: %d density: %g\n', j, n, curr_node, curr_cds); 
    assert((length(H.S) == H.ns)) ; 
    assert((H.ns == n - length(removed)), sprintf('H.ns: %d\n', H.ns) ); 
    assert(length(setdiff(remaining, H.S)) == 0); 
    
    assert(~ismember(curr_node, H.S)); 
end
