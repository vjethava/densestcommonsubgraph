function [S, S_density, result] = dcs_greedy_minimal(H)
% 
% GraphSet on which to run the greedy algorithm 
% 
m = H.m ; 
n = H.n; 
S_density = 0.0;
S = []; 
for j=1:(n-1)
    % common density of the current subgraph 
    [curr_cds, min_graph_idx] = min(H.density) ;     
    if curr_cds > S_density
        S_density = curr_cds;
        S = H.S; 
    end
    
    deg_of_min_deg_node = 0; 
    while(length(H.degree_list{deg_of_min_deg_node + 1, min_graph_idx}) == 0)
        deg_of_min_deg_node= deg_of_min_deg_node + 1; 
    end
    
    curr_deg_list = H.degree_list{deg_of_min_deg_node + 1, min_graph_idx}; 
    curr_node = curr_deg_list(1); 
    if mod(j, 1000) == 0
        fprintf(2, '%d/%d density: %g best: %g ns: %d\n', j, n, curr_cds, S_density, length(S)); 
    end
    
    H.remove_node(curr_node); 
end


fprintf(2, 'DCSGREEDY1: Extracted subgraph of size %d and density %g\n', length(S), S_density); 
