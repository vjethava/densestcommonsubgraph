function [t, result]=dcs_l1(A, lambda)
%%  DCS_L1 Solves Densest Common Subgraph (DCS) LP 
% by considering the following LP: 
% 
%          $$max_{x,y,t}  t$$ 
%          s.t.          
 %                       $$\sum_{ij\in E^m} x^{m}_{ij} >= t  \forall [m]$$ 
%                        $$x^{m}_{ij} <= z^{m}_{i}$$
%                        $$x^{m}_{ij} <= z^{m}_{j}  \forall ij\in E^{m}$$
%                        $$\sum_{i} z^{m}_{i} <= 1$$  
%                        $$x^{m}_{ij}, y^{m}_{i} >=  0$$
%                        $$sum_i | y^m_i - y_i | <= lambda$$ 
% 
% 
% RETURNS: 
% --------
% t: objective value of the LP
% result:  structure containing: 
%      result.x: primal variables
%      result.y: primal variables 
%      
if ~exist('cvx_version', 'file')
    error('dcs_l1(): Set-up CVX first!'); 
end

m = length(A); 
n = size(A{1}, 1); 
fprintf(2, 'starting LP setup for DCS_L1\n') ;
cvx_quiet false ;

for i=1:m
    if(isa(A{i}, 'Graph'))
        A{i} = double(A{i});
    end
end

        
        
cvx_begin 
variable x(n, n, m) nonnegative; 
variable z(n, m) nonnegative; 
variable y(n) nonnegative; 
variable t; 
maximize t
subject to 
sum(y) <= 1 ; 
for i=1:m
    sum(z(:, i)) <= 1 ; 
    trace(A{i} * x(:, :, i)) >= t; 
    [ai, aj, av] = find(A{i}); 
    for e=1:length(ai)
        crow = ai(e); 
        ccol = aj(e); 
        x(ai(e), aj(e), i) <= z(ai(e), i);
        x(ai(e), aj(e), i) <= z(aj(e), i);
    end
    sum(abs(y - z(:, i))) <= lambda; 
end
cvx_end

result = struct();
result.x = x;
result.y = y; 
result.z = z; 
