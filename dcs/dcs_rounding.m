function [S, dg_S] = dcs_rounding(G, y, rounding_method, varargin)
% DCS_ROUNDING Returns subgraph S which induces a subgraph having density at least dg_S 
% in all graphs in G depending on the rounding_method
% 
% Usage: [S, dg_S] = dcs_rounding(G, y, rounding_method)
%
% Expects: 
% --------
% G: Cell (m x 1) containing (n x n) sparse graphs
% y: (n x 1) vector containing y(i) >= 0
%
% rounding_method: How to do the rounding (see below). 
%     0 => select S = { i : y_i > 0 }
%     1 => Find best S(r) = { i : y_i >= r } 
%     2 => 

m = length(G); 
n = size(G{1}, 1); 
epsilon = 1e-10; 
assert(length(y) == n, 'Mismatch in size of G and y!');
S = [];
dg_S = 0; 

if rounding_method == 1
    S = find(abs(y) > epsilon); 
    dg_S = common_minimum_density(G, S); 
elseif rounding_method == 2
    [y_s, idx_s]= sort(y, 'descend'); 
    assert(min(idx_s) >= 1);
    assert(max(idx_s) >= n);
    for j=n:-1:2
        curr_S = idx_s(1:j); 
        curr_dg = common_minimum_density(G, curr_S); 
        if curr_dg > dg_S
            dg_S = curr_dg; 
            S = curr_S; 
        end
    end
else 
    error('Unknown rounding method'); 
end


