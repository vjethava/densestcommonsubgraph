function [dg_S] = common_minimum_density(G, S)
% Function that computes 
% 
% dg_S = \argmin_m |E^m(S)| / |S|
% 
% 
m = length(G);
n = size(G{1}, 1);
ns  = length(S);
dg_S = inf ; 
for i=1:m
    A = double(G{i}); 
    curr_density = sum(sum(A(S, S))) / (2 * ns);
    dg_S = min(dg_S, curr_density);
end



