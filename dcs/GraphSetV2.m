classdef GraphSetV2 < handle 
    properties
        G
        S
        node_degrees
        degree_list
        density
        max_degree 
    end
    
    properties 
        n
        m
        ns 
    end
    
    methods 
        function obj=GraphSetV2(G)
            obj.m = length(G);
            obj.n = size(G{1}, 1);
            obj.G = G;
            obj.S = [1:(obj.n)]';
            
            obj.ns = length(obj.S); 
            obj.update_degree_list(); 
            obj.density = cellfun(@(x) nnz(x(obj.S, obj.S))/(2*length(obj.S)), obj.G);
            
        end
            
        function [min_degree, min_graph_id] = get_min_degree_across_graphs(obj, node)
            min_degree = 0; min_graph_id = 0; 
 
            node_idx = find(obj.S == node); 
            [min_degree, min_graph_id] = min(obj.node_degrees(node_idx, :)); 
        end
        
        
        function [max_degree, max_graph_id] = get_max_degree_across_graphs(obj, node)
            max_degree = 0; max_graph_id = 0; 
            node_idx = find(obj.S == node); 
            [max_degree, max_graph_id] = max(obj.node_degrees(node_idx, :)); 
        end
        
        
        function update_degree_list(obj)             
            obj.degree_list = cell(obj.ns, obj.m); 
            obj.node_degrees = zeros(obj.ns, obj.m); 
            max_degree = 0; 
            for j=1:length(obj.S)
                if mod(j, 1000) == 0
                    fprintf('GraphSet.update_degree_list() node: %d/%d\n', j, length(obj.S));
                end
                c_node = obj.S(j);
                for k=1:obj.m  
                    obj.node_degrees(c_node, k) = nnz(obj.G{k}(obj.S, c_node)); 
                    c_degree =  obj.node_degrees(c_node, k);  
                    obj.degree_list{c_degree+1, k} = [obj.degree_list{c_degree+1, k} ; c_node];  
                    max_degree = max(c_degree, max_degree); 
                end
            end
            obj.degree_list = obj.degree_list(1:(max_degree+1), :); 
            obj.max_degree = max_degree; 

        end  
        
        
        function [min_deg, min_idx] = min_degree_set(obj)
            min_deg = zeros(obj.m, 1); 
            min_idx = cell(obj.m , 1); 
            for i=1:(obj.m) 
                j = 1; 
                while (j <= obj.ns) && (isempty(obj.degree_list{j, i}))
                    j =  j + 1;
                end
                min_deg(i) = j-1; 
                min_idx{i} = obj.degree_list{j, i};  
            end
            
        end
        
        function remove_node_from_degree_list(obj, node, d, m)
            curr_list = obj.degree_list{d+1, m}; 
            node_idx = find(curr_list == node); 
            assert(length(node_idx) == 1, 'This node should be present only once'); 
            obj.degree_list{d+1, m} = curr_list(curr_list ~= node);  
        end
        
        function add_node_to_degree_list(obj, node, d, m)
            curr_list = obj.degree_list{d+1, m}; 
            node_idx = find(curr_list == node); 
            assert(isempty(node_idx), 'This node should not already be present');  
            obj.degree_list{d+1, m} = [curr_list; node];
        end
        
        function decrement_node_in_degree_list(obj, node, old_deg, m)
            assert(old_deg > 0); 
            obj.remove_node_from_degree_list(node, old_deg, m) ;
            obj.add_node_to_degree_list(node, old_deg - 1 , m); 
        end
        
        function remove_node(obj, node)
            if ~ismember(node, obj.S)
                fprintf('Attempting to remove node not present in GraphSet.S - No action taken\n');  
                return 
            end
            m = obj.m; 
            n_idx = find(obj.S == node);
            assert(length(n_idx) == 1); 
            %        n_deg = obj.node_degrees(n_idx); 
            ngbrs = cell(obj.m, 1); 
            for k=1:(obj.m)
                ngbrs{k} = find(obj.G{k}(obj.S, node)); % improves performance 
                ne = length(ngbrs{k}); 
                obj.density(k) = ( obj.density(k) * obj.ns - ne ) / (obj.ns - 1); 
                
                % remove this node from degree_list        
                obj.remove_node_from_degree_list(node, ne , k); 
                
                % update neighbour degrees 
                for j=1:ne
                    c_idx = ngbrs{k}(j);
                    c_ngbr = obj.S(c_idx); 
                    prev_degree = obj.node_degrees(c_ngbr, k);   
                    obj.node_degrees(c_ngbr, k) = prev_degree - 1; 
                    obj.decrement_node_in_degree_list(c_ngbr, prev_degree, k); 
                end                          
            end
            
            new_ns = [1:(n_idx-1) (n_idx+1):obj.ns];
            obj.S = obj.S(new_ns); 
            % obj.node_degrees = obj.node_degrees(new_ns, :); 
            % for i=1:(obj.m)
            %     assert(isempty(obj.degree_list{obj.ns, i}) , 'No nodes of degree (ns-1) any more\n' ); 
            % end
            obj.ns = obj.ns - 1;      
            % obj.degree_list = obj.degree_list(1:(obj.ns), :); 

        end
    end
end

