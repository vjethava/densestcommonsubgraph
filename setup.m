clear all; close all; 

% Paths that are to be set-up 
DCS_HOME='~/git/densestcommonsubgraph/';
SVM_THETA_HOME='~/Code/svm-theta/'


% Setup SVM THETA
run([SVM_THETA_HOME '/setup.m']);

% Setup DCS 
cd(DCS_HOME);
addpath([DCS_HOME '/dcs/']);
addpath([DCS_HOME '/scripts/']);
