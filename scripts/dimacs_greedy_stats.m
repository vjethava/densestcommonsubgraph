clear all; close all; clc; 
load dimacs-orig.mat; 
ng = length(G); 
s_greedy = cell(ng, 1); 
ns_greedy = zeros(ng, 1); 
d_greedy = zeros(ng, 1); 
ub_greedy = zeros(ng, 1); 
for i=1:ng
    cG = G{i}; 
    [S, d, r, res] = dcs_greedy1(cG); 
    s_greedy{i} = S; 
    ns_greedy(i) = length(S); 
    d_greedy(i) = d; 
    ub_greedy = 2 * r * d;    
end

DimacsGreedyStats = table(s_greedy, ns_greedy, d_greedy, ub_greedy); 
