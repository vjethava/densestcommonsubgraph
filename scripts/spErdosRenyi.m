function G = spErdosRenyi (n, p)
% ERDOSRENYI Generate the erdos-renyi random graph G(n, p) 
%
% Usage: G = erdosRenyi (n, p)
%
% Returns
% -------
% G: the adjacency matrix for the generated graph
%
% Expects
% -------
% n: number of vertices in the graph 
% p: the edge probability
%

nl = round( n * (n+1)/2 * p ); % takes into account - a) elements on  diagonal b) repeated (i, j)
ii = fix( rand(nl, 1) * n ) + 1;
jj = fix( rand(nl, 1) * n ) + 1;
di = find( ii == jj );
off = find( ii ~= jj );
nd = length( di );
fprintf(1, 'nd: %d noff: %d exp: %g \n', nd, length(off), length(off)/(n*(n-1)*p/2) );
% randi = rands( 1:nd );
% rando = rands( nd+1 : nl );
i = [ii(off); jj(off)];
j = [jj(off); ii(off)];
s = ones(length(i), 1); 
% r = [rando; rando; randi];
G = sparse(i,j, s, n, n);
% assert(nnz(G) == length(s), 'error in matrix instantiation'); 