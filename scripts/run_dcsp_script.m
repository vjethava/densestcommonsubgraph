
% This file compares the densest common subgraph obtained using three approximate methods
%
% MKL (SVM-THETA)
% DCS-LP
% CHARIKAR(MIN)
% CHARIKAR(AVG)
% 

res_dir = [getenv('SVM_THETA_HOME') '/results/'];
dimacs_res_file = '/home/vjethava/dimacs-orig.mat'; 
load(dimacs_res_file); % G, alpha, 

% load([res_dir '/dimacs-mkl-normalized-laplacian.mat' ]); 
% cmp_res_file = [res_dir '/dimacs-mkl-laplacian-comparison.mat']; 

solvers = {'MKL'; 'DCSP' ;  'MKL-LAPLACIAN' }; % 1 => MKL
                                               % 2 => DCS

graph_names = DIMACS.graph_sets ;
rounding_methods = {'YPOS', 'GREEDY' };  % 1 choose S = {y_i > 0} 
                                         % 2 choose S greedy when sorted by y_i 


ng = length(graph_names);
ns = length(solvers); 
nr = length(rounding_methods); 

ng_to_run = [11 13];
parfor i=1:length(ng_to_run) 
  gi = ng_to_run(i)  % choose graph set and run DCSP 

     curr_graph_set = G{gi}; 
     curr_graph_set_name = graph_names{gi};     
fprintf(2, 'running DCSP for graph %s\n' , curr_graph_set_name); 
     [t, x, y, cvx_status] = DCSP(curr_graph_set); 
     curr_dcsp_file = [res_dir '/dcsp/' curr_graph_set_name '.mat'];
     parsave(curr_dcsp_file, t, x, y, cvx_status, curr_graph_set); 
end






