% This script runs the MKL vs DCS comparison for SNAP datasets 
clc; clear all; close all; 
fprintf(2, 'Running MKL vs DCSP comparison on SNAP datasets\n'); 
run([getenv('SVM_THETA_HOME') filesep 'setup.m']); 

data_dir = [getenv('SVM_THETA_HOME') '/data/dcs/' ]; 
res_dir =  [getenv('SVM_THETA_HOME') '/results/' ]; 
snap_dataset = 'Oregon-2'; 

if exist([data_dir filesep snap_dataset '.mat'], 'file') == 0
    error(['dataset ' snap_dataset '.mat not found in directory ' data_dir]);
end

load([data_dir filesep snap_dataset '.mat']);

G = Problem.aux.G;
% [obj, x, dcsp_y, status] = DCSP(G); 
% save([res_dir filesep snap_dataset '.mat']); 


cmp_res_file = [res_dir filesep snap_dataset '-results.mat']; 

solvers = {'MKL';  'DCS'; 'MKL-LAPLACIAN' }; % 1 => MKL
                                             % 2 => DCS

rounding_methods = {'YPOS', 'GREEDY' };  % 1 choose S = {y_i > 0} 
                                         % 2 choose S greedy when sorted by y_i 


ns = length(solvers); 
nr = length(rounding_methods); 


% saves the following information: graph_set, solver, y,  rounding, S, |S|, d , time_taken  
rounding_result = cell(ns * nr, 8); 

% do rounding and save in rounding_result 

ii = 0; 
for si=[1] % choose solver
    ii = ii + 1; 
    curr_id = ii; 
    curr_solver = solvers{si};
    if si == 1 % MKL solver
        tic; 
        curr_y = SimpleMKL.get_mkl_alpha(G, 'LS'); 
        curr_t = toc; 
    elseif si == 2  % DCSP 
        % dcs_res_file = [res_dir  '/dcsp/' curr_graph_set_name '.mat']; 
        % load(dcs_res_file, 'y'); 
        % curr_y = y; clear y; 
        % assert(length(curr_y) == size(curr_graph_set{1}, 1)); 
    elseif si == 3
        curr_y = SimpleMKL.get_mkl_alpha(G, 'LAPLACIAN'); 
    else            
        error('unknown solver'); 
    end
    % fprintf(2, '%s %s ny: %d\n', curr_graph_set_name, curr_solver, length(curr_y)); 
    for ri=1:nr % choose rounding 
        S = [];
        d = 0.0; 
        [S, d] = dcs_rounding(G, curr_y, ri); 
        rr_id = nr * (curr_id-1) + ri;  
            
        rounding_result{rr_id, 1} = snap_dataset;
        rounding_result{rr_id, 2} = curr_solver; 
        rounding_result{rr_id, 3} = curr_y;
        rounding_result{rr_id, 4} = rounding_methods{ri};             
        rounding_result{rr_id, 5} = S;
        rounding_result{rr_id, 6} =length(S);
        rounding_result{rr_id, 7} =  d; 
    end
end
fprintf(2, 'saving rounding_result of graph set %s to %s\n', snap_dataset, cmp_res_file); 
save(cmp_res_file, 'rounding_result'); 










